import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.Timeout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@Timeout(value = 10, unit = TimeUnit.MINUTES)
public class TestUserConnection
{
    private static Socket socketClient;
    private static Thread server;

    @BeforeAll
    public static void initializeServer()
    {
        server = new Thread(new Server());
        server.start();
    }

    @AfterAll
    public static void disconnectServer()
    {
        server.interrupt();
    }

    @BeforeEach
    public void display(TestInfo testInfo)
    {
        System.out.println("Running test : "+testInfo.getDisplayName());
    }

    @BeforeEach
    public void initializeSocket() throws Exception
    {
        socketClient = new Socket("localhost",5000);
        // welcome message
        String message = getNextLine(socketClient);
        Assertions.assertTrue(message.startsWith("+OK POP3 Server ready "));
        Assertions.assertTrue(checkRegex(message));
    }

    /*
        valid format :
        <1896.697170952@ABC.home.us>
        <3333.965432@Server.fr>
    */
    public static boolean checkRegex(String message)
    {
        Pattern regex = Pattern.compile("<[0-9]+\\.[0-9]+@([a-z]+\\.)+[a-z]+>");
        return regex.matcher(message).find();
    }

    @Test
    public void unknownCommand()
    {
        sendMessage(socketClient,"AEAEZZ zdzdzdz");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("-ERR inappropriate command"));
    }

    @Test
    public void quit()
    {
        sendMessage(socketClient,"QUIT");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK"));
    }

    @Test
    public void userConnectionFailed()
    {
        sendMessage(socketClient,"USER mlfdmskdfdmfskfm");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("-ERR user does not exist"));
        // without username
        sendMessage(socketClient,"USER");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("-ERR"));
    }

    @Test
    public void userConnectionSucceed()
    {
        sendMessage(socketClient,"USER merlot");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK merlot"));
        sendMessage(socketClient,"QUIT");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK"));
    }

    @Test
    public void userAndPasswordConnectionFailed()
    {
        sendMessage(socketClient,"USER merlot");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK merlot"));
        sendMessage(socketClient,"PASS password");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("-ERR password or username not valid"));
        sendMessage(socketClient,"PASS password");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("-ERR user have not be sent"));
        sendMessage(socketClient,"QUIT");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK"));
    }

    @Test
    public void userAndPasswordConnectionSucceed()
    {
        sendMessage(socketClient,"USER merlot");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK merlot"));
        sendMessage(socketClient,"PASS secret");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK merlot is connected"));
        sendMessage(socketClient,"USER merlot");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("-ERR you are already connected"));
        sendMessage(socketClient,"PASS dsfsdfsfsdf");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("-ERR you are already connected"));
        sendMessage(socketClient,"QUIT");
        Assertions.assertTrue(getNextLine(socketClient).startsWith("+OK"));
    }

    @Test
    public void userApopConnectionFailed()
    {
        //TODO
    }

    @AfterEach
    public void disconnect() throws IOException
    {
        socketClient.close();
    }

    public static String getNextLine(Socket socket)
    {
        try {
            return new BufferedReader(new InputStreamReader(socket.getInputStream())).readLine();
        } catch (Exception e) {
            return "";
        }
    }

    public void sendMessage(Socket socket,String content)
    {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
            writer.println(content);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
