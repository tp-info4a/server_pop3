import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;

import java.util.ArrayList;
import java.util.regex.Pattern;


public class TestUserMail
{
    private final String USER = "merlot";
    private final String PASSWORD = "secret";

    private UserList mailServer;

    public TestUserMail()
    {
        mailServer = new UserList();
    }

    @BeforeEach
    public void display(TestInfo testInfo)
    {
        System.out.println("Running test : "+testInfo.getDisplayName());
    }

    @Test
    public void getUserMailFailed()
    {
        Assertions.assertNull(mailServer.getUser("adbaadlajkada"));
        Assertions.assertNotNull(mailServer.getUser("merlot"));
    }

    @Test
    public void addUser()
    {
        /*
        mailServer.createUser("hello","world");
        Assertions.assertNotNull(mailServer.getUser("hello"));
        Assertions.assertTrue(mailServer.getUser("hello").getUserMails().getListMails().isEmpty());
        Assertions.assertEquals(mailServer.getUser("hello").getPasswordText(),"world");
         */
    }

    @Test
    public void addUserFailed()
    {
        // To Do, user already exists
    }

    @Test
    public void addAndReadMail()
    {

    }

    @Test
    public void listMails()
    {
        ArrayList<String> aS = mailServer.getUser(USER).mailList();
        String message = aS.get(0);
        Pattern regex = Pattern.compile("\\+OK [0-9]+ messages \\([0-9]+ octets\\)");
        Assertions.assertTrue(regex.matcher(message).find());
    }
}
