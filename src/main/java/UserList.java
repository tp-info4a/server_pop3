import org.w3c.dom.*;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;

public class UserList
{
    private ArrayList<User> listUsers;

    public UserList()
    {
        try
        {
            listUsers = new ArrayList<User>();
            loadUsers();
        }
        catch(Exception e)
        {
            System.out.println("Couldn't load file");
        }
    }

    public void loadUsers() throws Exception
    {
        // Getting resource(File) from class loader
        File file = new File( this.getClass().getResource( "usersdata.xml" ).toURI());
        //File file = getFileFromResources("usersdata.xml");

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);
        NodeList userNodes = document.getElementsByTagName("user");
        int temp;
        for (temp = 0; temp < userNodes.getLength(); temp++)
        {
            Node nNode = userNodes.item(temp);
            Element userElement = (Element) nNode;
            createUser(userElement.getElementsByTagName("username").item(0).getTextContent(), userElement.getElementsByTagName("password").item(0).getTextContent());
        }
    }

    public User getUser(String username)
    {
        for (User user : listUsers)
        {
            if (user.getUsername().equals(username))
                return user;
        }
        return null;
    }

    public void createUser(String username, String password)
    {
        listUsers.add(new User(username, password));
    }

    public boolean userExists(String username)
    {
        User us = getUser(username);
        if(us != null)
            return true;
        else return false;
    }

    public boolean isUserConnected(String username)
    {
        User us = getUser(username);
        if(us != null && us.isConnected())
            return true;
        else return false;
    }

    public boolean isUserConnected(User user)
    {
        if(user != null && user.isConnected())
            return true;
        else return false;
    }

    /**
     * Checks if the info provided by the client correspond to an existing user
     * @param username provided by the client
     * @param passwordMD5 the hashed password provided by the client
     * @param timestamp the PID, timestamp and hostname, concatenated, used by the hashing algorithm
     * @return
     */
    public boolean verifyUserAPOP(String username, String passwordMD5, String timestamp)
    {
        for (User user : listUsers)
        {
            if (user.getUsername().equals(username))
            {
                return validatePasswordMD5(timestamp, passwordMD5, user.getPasswordText());
            }
        }
        return false;
    }

    public boolean verifyUserUP(String username, String passwordProvided)
    {
        for (User user : listUsers)
        {
            if (user.getUsername().equals(username))
            {
                return validatePasswordText(passwordProvided, user.getPasswordText());
            }
        }
        return false;
    }

    public boolean validatePasswordText(String passwordProvided, String passwordStored)
    {
        return passwordProvided.equals(passwordStored);
    }

    /**
     * This function checks if the password provided by the user is the same as the one stored
     * @param timestamp the PID, timestamp and hostname, concatenated, used by the hashing algorithm
     * @param passwordMD5 the hash sent by the user. (timestamp + password).hashedByMD5
     * @param passwordText the password stored by the server, in clear text
     * @return true if the password match
     */
    public static boolean validatePasswordMD5(String timestamp, String passwordMD5, String passwordText)
    {
        String sumToMD5 = timestamp + passwordText;
        byte[] bytesOfUserProvidedMD5 = hexStringToByteArray(passwordMD5);
        byte[] bytesOfServerProvided = sumToMD5.getBytes(StandardCharsets.UTF_8);

        MessageDigest md = null;
        try
        {
            md = MessageDigest.getInstance("MD5");
        }
        catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        byte[] bytesOfServerProvidedMD5;
        if(md != null)
        {
            bytesOfServerProvidedMD5= md.digest(bytesOfServerProvided);
        }
        else
        {
            return false;
        }
        return diffBytes(bytesOfServerProvidedMD5, bytesOfUserProvidedMD5);
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    public static boolean diffBytes(byte[]  hash1, byte[] hash2)
    {
        int diff = hash1.length ^ hash2.length;
        for(int i = 0; i < hash1.length && i < hash2.length; i++)
        {
            diff |= hash1[i] ^ hash2[i];
        }
        return diff == 0;
    }

    //This shit can't be used with APOP
    // Stored here for later
    public String createHashPBKDF2(String password) throws NoSuchAlgorithmException, InvalidKeySpecException {
        int iterations = 10000;
        char[] chars = password.toCharArray();
        byte[] salt = getSalt();

        PBEKeySpec spec = new PBEKeySpec(chars, salt, iterations, 64 * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] hash = skf.generateSecret(spec).getEncoded();
        return "$pbkdf2$" + iterations + "$" + toHex(salt) + "$" + toHex(hash);
    }

    private static byte[] getSalt() throws NoSuchAlgorithmException
    {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    private static String toHex(byte[] array) throws NoSuchAlgorithmException
    {
        BigInteger bi = new BigInteger(1, array);
        String hex = bi.toString(16);
        int paddingLength = (array.length * 2) - hex.length();
        if(paddingLength > 0)
        {
            return String.format("%0"  +paddingLength + "d", 0) + hex;
        }
        else
            {
            return hex;
        }
    }

    public static boolean validatePasswordPBKDF2(String originalPassword, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException
    {
        String[] parts = storedPassword.split("\\$");
        int iterations = Integer.parseInt(parts[2]);
        byte[] salt = fromHex(parts[3]);
        byte[] hash = fromHex(parts[4]);

        PBEKeySpec spec = new PBEKeySpec(originalPassword.toCharArray(), salt, iterations, hash.length * 8);
        SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        byte[] testHash = skf.generateSecret(spec).getEncoded();

        return diffBytes(hash, testHash);
    }

    private static byte[] fromHex(String hex) throws NoSuchAlgorithmException
    {
        byte[] bytes = new byte[hex.length() / 2];
        for(int i = 0; i<bytes.length ;i++)
        {
            bytes[i] = (byte)Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
        }
        return bytes;
    }
}
