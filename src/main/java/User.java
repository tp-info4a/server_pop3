import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class User
{
    private String username;
    private String passwordText;
    private boolean connected;
    private MailList userMails;

    public MailList getUserMails() {
        return userMails;
    }

    public boolean isConnected() {
        return connected;
    }

    public void setConnected(boolean connected) {
        this.connected = connected;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordText() {
        return passwordText;
    }

    public void setPasswordText(String passwordText) {
        this.passwordText = passwordText;
    }

    public User(String user, String pass)
    {
        username = user;
        passwordText = pass;
        userMails = new MailList(username, getFolder());
    }

    /**
     *
     * @return the number of emails of this user
     */
    public int getNumberOfMails()
    {
        return userMails.getListMails().size();
    }

    /**
     *
     * @return the size in octets of all emails of this user
     */
    public long getTotalMailsSize()
    {
        long size = 0;
        File directory = getFolder();
        File[] mails;
        if (directory != null)
        {
            mails = directory.listFiles();
            if(mails != null)
            {
                for (File mail:mails)
                    size += mail.length();
            }
        }
        return size;
    }

    /**
     *
     * @return a File object representing this user's folder containing his emails
     */
    public File getFolder()
    {
        String directory = "/" + username + "/";
        File dirUser;
        // Getting resource(File) from class loader
        try
        {
            dirUser = new File( this.getClass().getResource( directory ).toURI());
        }
        catch (URISyntaxException e)
        {
            return null;
        }
        return dirUser;
    }

    /**
     *
     * @param mailNumber the email identifier
     * @return a File object of a specified email
     */
    public File getMailFile(int mailNumber)
    {
        String filename = "/" + username + "/" + username + mailNumber + ".xml";
        File mailFile;
        // Getting resource(File) from class loader
        try
        {
            mailFile = new File( this.getClass().getResource( filename ).toURI());
        }
        catch (URISyntaxException e)
        {
            return null;
        }
        return mailFile;
    }

    /**
     *
     * @return a String containing the number of mails and their total size
     */
    public String mailStat()
    {
        int numberOfMails = 0;
        long sizeOfMails = 0;
        for(Mail m: userMails.getListMails())
        {
            if(!userMails.getMailToDelete().contains(m))
            {
                sizeOfMails += m.getSize();
                numberOfMails++;
            }
        }
        return "+OK " + numberOfMails + " " + sizeOfMails;
    }

    /**
     *
     * @return an ArrayList<String> containing each mail's (not added to the delete list) size and ID
     */
    public ArrayList<String> mailList()
    {
        int numberOfMails = 0;
        long sizeOfMails = 0;
        ArrayList<String> aS = new ArrayList<String>();
        for(Mail m : userMails.getListMails())
        {
            if(!userMails.getMailToDelete().contains(m))
            {
                sizeOfMails += m.getSize();
                numberOfMails++;
                String s = m.getMailIdentifier() + " " + m.getSize();
                aS.add(s);
            }
        }
        String resume = "+OK " + numberOfMails + " messages ("+ sizeOfMails + " octets)";
        aS.add(0, resume);
        return aS;
    }

    /**
     *
     * @param mailID mail identifier
     * @return a String containing the mail's size and ID or an error
     */
    public String mailList(long mailID)
    {
        for(Mail m : userMails.getListMails())
        {
            if(!userMails.getMailToDelete().contains(m) && mailID == m.getMailIdentifier())
            {
                return "+OK " + mailID + " " + m.getSize();
            }
        }
        return "-ERR no such message, only " + userMails.getListMails().size() + " messages in mailbox";
    }

    public void addToDeleteMailList(long mailID)
    {
        userMails.addToDeleteList(userMails.getMail(mailID));
    }
}
