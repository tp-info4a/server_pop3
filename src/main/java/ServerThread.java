import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.lang.management.ManagementFactory;
import java.net.Socket;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.nio.charset.StandardCharsets;

public class ServerThread implements Runnable
{

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    private UserList uList;
    private User user;
    private boolean running = true;
    private static final String HOSTNAME = "server.xyz";
    private State currentState;
    private String username;

    public enum State{
        NOT_CONNECTED, AUTHORIZATION, WAITING_PASSWORD, TRANSACTION
    }

    public ServerThread(Socket s, UserList list)
    {
        currentState = State.NOT_CONNECTED;
        socket = s;
        uList = list;
        try
        {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));
            out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    public void run()
    {
        String pid = ManagementFactory.getRuntimeMXBean().getName();
        long timestamp = System.currentTimeMillis();
        String preSumControl = "<" + pid.split("@")[0] + "." + timestamp + "@" + HOSTNAME + ">";
        out.print("+OK POP3 Server ready ");
        out.println(preSumControl);
        out.flush();
        currentState = State.AUTHORIZATION;
        String line;
        while (running)
        {
            try
            {
                line = in.readLine();
                if (line != null)
                {
                    String event = (line.split(" ").length != 0) ? line.split(" ")[0] : "";
                    switch (event) {
                        case "APOP":
                            apop(preSumControl, line);
                            break;
                        case "USER":
                            userEventSwitch(line);
                            break;
                        case "PASS":
                            passEventSwitch(line);
                            break;
                        case "RETR":
                            retrEventSwitch(line);
                            break;
                        case "LIST":
                            listEventSwitch(line);
                            break;
                        case "STAT":
                            statEventSwitch();
                            break;
                        case "NOOP":
                            noopEventSwitch();
                            break;
                        case "DELE":
                            deleEventSwitch(line);
                            break;
                        case "QUIT":
                            quitEventSwitch();
                            break;
                        default:
                            sendMessage("-ERR inappropriate command");
                            break;
                    }
                }
            }
            catch (RuntimeException | IOException e)
            {
                running = false;
            }
        }
    }

    public void userEventSwitch(String line)
    {
        switch(currentState)
        {
            case AUTHORIZATION:
                String username = (line.split(" ").length > 1) ? line.split(" ")[1] : "";
                if(uList.userExists(username))
                {
                    userPassConnection(username);
                }
                else
                {
                    sendMessage("-ERR user does not exist");
                }
                break;
            case TRANSACTION:
                sendMessage("-ERR you are already connected");
                break;
            default:
                sendMessage("-ERR");
                break;
        }
    }

    public void passEventSwitch(String line)
    {
        switch(currentState)
        {
            case WAITING_PASSWORD:
                if(uList.isUserConnected(username))
                {
                    sendMessage("-ERR mailbox can't be locked");
                }
                else
                {
                    passwordAttempt(line);
                }
                break;
            case AUTHORIZATION:
                sendMessage("-ERR user have not be sent");
                break;
            case TRANSACTION:
                sendMessage("-ERR you are already connected");
                break;
            default:
                sendMessage("-ERR");
                break;
        }
    }

    public void retrEventSwitch(String line)
    {
        int messageId = Integer.parseInt(line.split(" ")[1]);
        File file = null;
        try {
            file = new File( this.getClass().getResource(user.getUsername() + "/" + user.getUsername() + messageId + ".xml").toURI());
            DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            Document document = documentBuilder.parse(file);
            NodeList headersList = document.getElementsByTagName("headers");
            NodeList mailContentList = document.getElementsByTagName("mailContent");
            Element elem = null;

            if (headersList.getLength() != 1)
                throw new Exception("Le message à pas le bon nombre d'header : " + headersList.getLength());

            if (mailContentList.getLength() != 1)
                throw new Exception("Le message à pas le bon nombre de contenus : " + headersList.getLength());

            // HEADERS
            Node header = headersList.item(0);
            for (int i = 0 ; i < header.getChildNodes().getLength() ; i++) {
                Node item = header.getChildNodes().item(i);

                if (item instanceof Element) {
                    elem = (Element) item;
                    out.println(elem.getNodeName()+":"+elem.getTextContent());
                    out.flush();
                }
            }
            out.println("");
            out.flush();
            // MAIL
            Node mailContent = mailContentList.item(0);
            for (int i = 0 ; i < mailContent.getChildNodes().getLength() ; i++) {
                Node item = mailContent.getChildNodes().item(i);

                if (item instanceof Element) {
                    elem = (Element) item;
                    // System.out.println(elem.getTextContent());
                    out.println(elem.getTextContent());
                    out.flush();
                }
            }
            out.println(".");
            out.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  This function adds the specified mail to the list of mails to delete when the user disconnects
     * @param line command + mailID
     */
    public void deleEventSwitch(String line)
    {
        switch (currentState)
        {
            case TRANSACTION:
                if (line.split(" ").length == 2 && line.split(" ")[1].matches("^\\d+$") &&
                        user.mailList(Long.parseLong(line.split(" ")[1])).startsWith("+OK"))
                {
                    user.addToDeleteMailList(Long.parseLong(line.split(" ")[1]));
                    sendMessage("+OK message deleted");
                }
                else sendMessage("-ERR invalid message number");
                break;
            default:
                sendMessage("-ERR");
                break;
        }
    }

    public void listEventSwitch(String line)
    {
        switch (currentState)
        {
            case TRANSACTION:
                if (line.split(" ").length == 1)
                {
                    ArrayList<String> aS = user.mailList();
                    for(String s : aS)
                    {
                        sendMessage(s);
                    }
                }
                else if (line.split(" ").length == 2 && line.split(" ")[1].matches("^\\d+$"))
                {
                    sendMessage(user.mailList(Long.parseLong(line.split(" ")[1])));
                }

                sendMessage(".");
                break;
            default:
                sendMessage("-ERR");
        }
    }

    public void statEventSwitch()
    {
        switch (currentState)
        {
            case TRANSACTION:
                sendMessage(user.mailStat());
                break;
            case AUTHORIZATION:
            case WAITING_PASSWORD:
                sendMessage("-ERR you are not connected");
                break;
            default:
                sendMessage("-ERR");
                break;
        }
    }

    public void quitEventSwitch()
    {
        switch(currentState)
        {
            case AUTHORIZATION:
            case WAITING_PASSWORD:
                quit();
                break;
            case TRANSACTION:
                if(user.getUserMails().getMailToDelete().isEmpty())
                {
                    quit();
                }
                else
                {
                    if (user.getUserMails().deleteMails())
                    {
                        sendMessage("+OK messages deleted");
                    }
                    else
                    {
                        sendMessage("-ERR some mails have not been deleted");
                    }
                    disconnect();
                }
                break;
            default:
                sendMessage("-ERR");
                break;
        }
    }

    public void noopEventSwitch()
    {
        switch(currentState)
        {
            case TRANSACTION:
                sendMessage("+OK");
                break;
            default:
                sendMessage("-ERR");
        }
    }

    /**
     * This function deals with the APOP connection. It handles the password verification, mailbox stat and
     * subsequent answer.
     * @param preSumControl the PID, timestamp and hostname, concatenated, used by the hashing algorithm
     * @param line the string containing what the client provided
     */
    private void apop(String preSumControl, String line)
    {
        if (line.split(" ").length == 3)
        {
            String username = line.split(" ")[1];
            String passwordMD5 = line.split(" ")[2];
            String answer;
            if (uList.verifyUserAPOP(username, passwordMD5, preSumControl))
            {
                user = uList.getUser(username);
                long emailsNumber = user.getNumberOfMails();
                long emailsSize = user.getTotalMailsSize();
                if(uList.isUserConnected(username))
                {
                    answer = "-ERR mailbox can't be locked";
                }
                else
                {
                    answer = "+OK mailbox has " + emailsNumber + " message (" + emailsSize + " octets)";
                    currentState = State.TRANSACTION;
                }
            }
            else
            {
                answer = "-ERR";
            }
            out.println(answer);
            out.flush();
        }
        else
        {
            sendMessage("-ERR password or username not valid");
        }
    }

    /**
     * This functions handles the USER/PASS connection combo. It verifies the password, mailbox stat and
     * subsequent answers.
     */
    private void userPassConnection(String username)
    {
        this.username = username;
        sendMessage("+OK " + username);
        currentState = State.WAITING_PASSWORD;
    }

    /**
     * This function handles the step after the command USER. Either a QUIT or a PASS. It verifies the password
     * provided is valid and checks if the user is already connected
     */
    private void passwordAttempt(String line)
    {
        String answer;
        String password = line.replaceFirst("PASS ", "");
        if(uList.verifyUserUP(username, password))
        {
            answer = "+OK " + username + " is connected";
            sendMessage(answer);
            user = uList.getUser(username);
            currentState = State.TRANSACTION;
        }
        else
        {
            answer = "-ERR password or username not valid";
            sendMessage(answer);
            currentState = State.AUTHORIZATION;
            //TODO: how many times do we allow the client to give a password?
            // running = false;
        }
    }

    public void quit()
    {
        sendMessage("+OK");
        disconnect();
    }

    public void disconnect()
    {
        running = false;
        user.setConnected(false);
        try
        {
            socket.close();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    private void sendMessage(String content)
    {
        out.println(content);
        out.flush();
    }

}
