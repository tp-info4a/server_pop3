import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

public class Mail
{
    private String username;
    private long mailIdentifier;
    private String headerSubject = null;
    private String headerFrom = null;
    private String headerTo = null;
    private String content = null;
    private File mailFile = null;

    public File getMailFile() {
        return mailFile;
    }

    public void setMailFile(File mailFile) {
        this.mailFile = mailFile;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getMailIdentifier()
    {
        return mailIdentifier;
    }

    public void setMailIdentifier(long mailIdentifier)
    {
        this.mailIdentifier = mailIdentifier;
    }

    public String getHeaderSubject()
    {
        return headerSubject;
    }

    public void setHeaderSubject(String headerSubject)
    {
        this.headerSubject = headerSubject;
    }

    public String getHeaderFrom()
    {
        return headerFrom;
    }

    public void setHeaderFrom(String headerFrom)
    {
        this.headerFrom = headerFrom;
    }

    public String getHeaderTo()
    {
        return headerTo;
    }

    public void setHeaderTo(String headerTo)
    {
        this.headerTo = headerTo;
    }

    public String getContent()
    {
        return content;
    }

    public void setContent(String content)
    {
        this.content = content;
    }


    public Mail()
    {
        mailIdentifier = 0;
    }

    /**
     * This function parses the XML file containing the email and extracts the text content
     * @param user owner of the email
     * @param mailID identifier of the email
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     * @throws URISyntaxException
     */
    public Mail(String user, long mailID) throws ParserConfigurationException, IOException, SAXException, URISyntaxException
    {
        setMailIdentifier(mailID);
        setUsername(user);
        File file;
        String pathname = "/" + username + "/" + username + mailID + ".xml";
        file = new File( this.getClass().getResource(pathname).toURI());

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(file);

        Node headers = document.getElementsByTagName("headers").item(0);
        NodeList headersList = headers.getChildNodes();
        for (int i = 0, len = headersList.getLength(); i < len; i++)
        {
            Node hNode = headersList.item(i);
            if (hNode.getNodeType() == Node.ELEMENT_NODE)
            {
                switch(hNode.getNodeName())
                {
                    case "subject":
                        setHeaderSubject(hNode.getTextContent());
                        break;
                    case "from":
                        setHeaderFrom(hNode.getTextContent());
                        break;
                    case "to":
                        setHeaderTo(hNode.getTextContent());
                        break;
                    default:
                        break;
                }
            }
        }

        Node content = document.getElementsByTagName("mailContent").item(0);
        if (content.getNodeType() == Node.ELEMENT_NODE)
        {
            setContent(content.getTextContent());
        }

        setMailFile(file);
    }

    /**
     * This function parses the XML file containing the email and extracts the text content
     * @param mail a File object containing an email
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     * @throws URISyntaxException
     */
    public Mail(File mail) throws ParserConfigurationException, IOException, SAXException, URISyntaxException
    {
        String filename = mail.getName();
        String[] f = filename.split("\\.");
        setUsername(f[0].substring(0, f[0].length() -1));
        setMailIdentifier(Long.parseLong(f[0].substring(f[0].length()-1)));
        setMailFile(mail);

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse(mail);

        Node headers = document.getElementsByTagName("headers").item(0);
        NodeList headersList = headers.getChildNodes();
        for (int i = 0, len = headersList.getLength(); i < len; i++)
        {
            Node hNode = headersList.item(i);
            if (hNode.getNodeType() == Node.ELEMENT_NODE)
            {
                switch(hNode.getNodeName())
                {
                    case "subject":
                        setHeaderSubject(hNode.getTextContent());
                        break;
                    case "from":
                        setHeaderFrom(hNode.getTextContent());
                        break;
                    case "to":
                        setHeaderTo(hNode.getTextContent());
                        break;
                    default:
                        break;
                }
            }
        }

        Node content = document.getElementsByTagName("mailContent").item(0);
        if (content.getNodeType() == Node.ELEMENT_NODE)
        {
            setContent(content.getTextContent());
        }
    }

    public boolean deleteMail()
    {
        File file;
        String pathname = "/" + username + "/" + username + mailIdentifier + ".xml";
        try
        {
            file = new File( this.getClass().getResource(pathname).toURI());
        }
        catch (URISyntaxException e)
        {
            return false;
        }
        return file.delete();
    }

    /**
     *
     * @return the size in octets of a specific email
     */
    public long getSize()
    {
        return mailFile.length();
    }
}