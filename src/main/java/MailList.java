import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class MailList
{
    private ArrayList<Mail> listMails;
    private ArrayList<Mail> mailToDelete;


    public ArrayList<Mail> getListMails() {
        return listMails;
    }

    public ArrayList<Mail> getMailToDelete() {
        return mailToDelete;
    }

    public MailList(String username, File folder)
    {
        listMails = new ArrayList<Mail>();
        mailToDelete = new ArrayList<Mail>();
        File[] mails = folder.listFiles();
        if(mails != null)
        {
            for(File f : mails)
            {
                if (f.getName().startsWith(username))
                {
                    try
                    {
                        listMails.add(new Mail(f));
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public Mail getMail(long mailID)
    {
        for (Mail m: listMails)
        {
            if(m.getMailIdentifier() == mailID)
                return m;
        }
        return null;
    }

    public void addToDeleteList(Mail mail)
    {
        mailToDelete.add(mail);
    }

    public boolean deleteMails()
    {
        for (Mail mail : mailToDelete)
        {
            if(!mail.deleteMail())
            {
                return false;
            }
        }
        return true;
    }
}
