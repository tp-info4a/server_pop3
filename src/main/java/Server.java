import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server implements Runnable
{
    private ServerSocket sSocket;

    public Server()
    {
        try
        {
            sSocket = new ServerSocket(5000);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        System.out.println("Server");
    }

    public void run()
    {
        while(true)
        {
            try
            {
                Socket s = sSocket.accept();
                UserList uList = new UserList();

                new Thread(new ServerThread(s, uList)).start();
                System.out.println("creation ServerThread - new client");
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }
}
